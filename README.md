# thrdmonitor

Multithreading asynchronous framework designed back in 2005 on java 1.4, for a specific project and finally made public.
Framework is using Java event thread to dispatch asynchronous event, log information and exceptions.
There is embeded GUI application that can be used to monitor thread executions in real time.
