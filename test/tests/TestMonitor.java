package tests;

import junit.framework.*;
import com.cubeprogramming.thrdmonitor.*;
import com.cubeprogramming.utils.*;

public class TestMonitor
    extends TestCase implements Constants {
  private ThreadGroup testThrdGrp = new ThreadGroup("testThrdGrp");
  private Monitor monitor = Monitor.getMonitor(testThrdGrp);

  public TestMonitor(String name) {
    super(name);
  }

  class TestThrd extends Thread {
    public TestThrd(ThreadGroup thrdGrp,String name) {
      super(thrdGrp,name);
      this.start();
    }

    public void run() {
      try {
        //sleep(Math.round(Math.random() * 10000));
        sleep(3000);
        if (this.getName().indexOf("notify") != -1) {
          ThreadEvent exitEvent = new ThreadEvent(this,EXIT_SUCCESS);
          System.out.println("Notification send: " + this.toString());
          Monitor.getMonitor(this.getThreadGroup()).notifyListeners(exitEvent);
        } else {
          ExitException exitException = new ExitException(EXIT_FAILURE, "failure");
          ThreadEvent exitEvent = new ThreadEvent(this,exitException);
          System.out.println("Notification send: " + this.toString());
          Monitor.getMonitor(this.getThreadGroup()).notifyListeners(exitEvent);
          //System.exit(Globals.EXIT_IOERR);
        }
        //sleep(3000);
      }catch (InterruptedException e){
        System.out.println("Nit je dobila interrupted signal");
      }
      System.out.println("Thread finished: " + this.toString());
    }//~run()
  }//~class TestThrd

  protected void setUp() throws Exception {
    super.setUp();
    //Instanciram niti
    //int i = 1;
    for (int i= 1; i < 10; i++) {
      new TestThrd(testThrdGrp, "notify-" + Integer.toString(i));
      new TestThrd(testThrdGrp, "interrupt-" + Integer.toString(i));
    }
  }//~setUp()

  protected void tearDown() throws Exception {
    super.tearDown();
  }//~tearDown()

  public void testThreadListener() throws Exception {
    monitor.addThreadListener(
      new ThreadListener(){
         public void notified(ThreadEvent thrdEvent) {
           if (thrdEvent.getSourceThread().getName().indexOf("notify") != -1) {
             int expectedReturn = EXIT_SUCCESS;
             int actualReturn = thrdEvent.getExitStatus();
             assertEquals("Exit Status", expectedReturn, actualReturn);
             System.out.println("Notification received: " + thrdEvent.toString());
           }else {
             assertTrue(thrdEvent.isExitException());
             int expectedReturn = EXIT_FAILURE;
             int actualReturn = thrdEvent.getExitStatus();
             assertEquals("Exception Status", expectedReturn, actualReturn);
             System.out.println("Notification received: " + thrdEvent.toString());
             //throw thrdEvent.getThreadException();
             //System.exit(Globals.EXIT_IOERR);
           }
         }
      } );
    monitor.waitFor();
    //System.exit(Globals.EXIT_IOERR);
  }//~testThreadListener()

}//~class TestMonitor
