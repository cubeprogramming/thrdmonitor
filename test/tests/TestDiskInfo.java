package tests;

import junit.framework.*;
import com.cubeprogramming.utils.*;

public class TestDiskInfo
    extends TestCase {
  private DiskInfo diskInfo = null;

  public TestDiskInfo(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    diskInfo = DiskInfo.getDiskInfo();
  }

  protected void tearDown() throws Exception {
    diskInfo = null;
    super.tearDown();
  }

  public void testGetKbFree() {
    long actualReturn = diskInfo.getKbFree();
    assertTrue(actualReturn > 0);
  }

}
