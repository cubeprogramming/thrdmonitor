package tests;

import junit.framework.*;

public class AllTests
    extends TestCase {

  public AllTests(String s) {
    super(s);
  }

  public static Test suite() {
    TestSuite suite = new TestSuite();
    suite.addTestSuite(TestFileUtil.class);
    suite.addTestSuite(TestThreadEvent.class);
    suite.addTestSuite(TestMonitor.class);
    return suite;
  }
}//~class AllTests
