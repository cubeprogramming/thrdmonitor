package tests;

import junit.framework.*;
import java.io.*;
import com.cubeprogramming.utils.*;

public class TestFileUtil
    extends TestCase {
  private FileUtil fileUtil = null;

  public TestFileUtil(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    fileUtil = new FileUtil();
  }

  protected void tearDown() throws Exception {
    fileUtil = null;
    super.tearDown();
  }

  public void testCopy() throws IOException {
    File src = new File("d:\\home\\cube\\Projects\\cpp\\cdrs\\cdrin\\TT10062_UDR01_20030901_092749.txt");
    File dest = new File("d:\\home\\cube\\Projects\\cpp\\cdrs\\cdrbak\\TT10062_UDR01_20030901_092749.txt");
    int expectedReturn = 0;
    int actualReturn = 1;
    try {
      actualReturn = fileUtil.copy(src, dest);
    }
    catch (Exception e) {
      fail(e.getMessage());
    }
    assertEquals("return value", expectedReturn, actualReturn);
  }//~testCopy()

}//~class TestFileUtil
