package tests;

import java.util.*;
import java.io.*;
import com.cubeprogramming.thrdmonitor.*;
import com.cubeprogramming.utils.*;
import com.cubeprogramming.logs.*;

public class TestMonitorMain implements ThreadListener, FreeResources, Constants{
  private ThreadGroup testThrdGrp = new ThreadGroup("testThrdGrp");
  private Monitor monitor = Monitor.getMonitor(testThrdGrp);
  public MainLog mainLog;
  private int expectedThrdNum, currThrdNum = 0;//Trenutni i ocekivani broj niti u programu

  int startCount = 0;

  class TestThrd extends Thread {
    private BufferedLogFile logFile;

    public TestThrd(ThreadGroup thrdGrp,String name) {
      super(thrdGrp,name);
      this.start();
    }

    public void run() {
      try {
        Monitor monitor = Monitor.getMonitor(this.getThreadGroup());
        monitor.monitorFrame.writeAnalysisLog("Thread run: " + this.toString()
                                                   + " Count: " + ++startCount);
        String logPath = "D:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\logs\\" +
                         this.toString() + ".log";

        try{
          //Otvara log datoteku
        logFile = new BufferedLogFile(new File(logPath));

        logFile.write(startCount,this.toString());
        }catch (IOException e) {
                    e.printStackTrace();
        }

        sleep(Math.round(Math.random() * 10000));
        //sleep(3000);
        if (this.getName().indexOf("notify") != -1) {
          ThreadEvent exitEvent = new ThreadEvent(this,EXIT_SUCCESS);
          System.out.println("Notification send: " + this.toString());
          monitor.notifyListeners(exitEvent);
        } else {
          ExitException exitException = new ExitException(EXIT_FAILURE, "failure");
          ThreadEvent exitEvent = new ThreadEvent(this,exitException);
          System.out.println("Notification send: " + this.toString());
          monitor.notifyListeners(exitEvent);
        }
        logFile.close();
      }catch (InterruptedException e){
        System.err.println("Nit je dobila interrupted signal");
        System.err.flush();
      }
      System.out.println("Thread finished: " + this.toString());
    }//~run()
  }//~class TestThrd

  public static void main(String[] argv) throws Exception {
    TimeZone.setDefault(GMT);
    dateFormat.setTimeZone(CET);
    System.out.println("**** Tarifiranje startano u: " +
                           dateFormat.format(new java.util.Date( ) ) + " ****");
    new TestMonitorMain();
    //System.exit(Globals.EXIT_IOERR);
    //System.setProperty("EXIT_STATUS", Integer.toString(Main.exitStatus));
    //System.err.println(System.getenv("EXIT_STATUS"));
    //System.getProperties().list(System.err);
  }//~main()

  int notfyCount = 0;
  public TestMonitorMain() {
    monitor.monitorFrame.setOutput(true);
    monitor.monitorFrame.setUI(true);
    monitor.monitorFrame.setLogPath(TestStartMonitorFrame.LOG_DIR);
    monitor.monitorFrame.setLog(true);
    monitor.setFreeObject(this);
    monitor.setJoinTimeout(7);
    //Registriram cekac na odgovor
    monitor.addThreadListener(
      new ThreadListener(){
         public void notified(ThreadEvent thrdEvent) {
           if (thrdEvent.getSourceThread().getName().indexOf("notify") != -1) {
             System.out.println("Notification received: " + thrdEvent.toString()
                                + " Count: " + ++notfyCount);
             /*monitor.monitorFrame.writeAnalysisLog("Notification received: " + thrdEvent.toString()
                                                   + " Count: " + notfyCount); */
           }else {
             System.out.println("Notification received: " + thrdEvent.toString()
                                + " Count: " + ++notfyCount);
           /*  monitor.monitorFrame.writeAnalysisLog("Notification received: " + thrdEvent.toString()
                                                   + " Count: " + notfyCount); */
           }
         }
      } );

     monitor.addThreadListener(this);

     //Formira direktorij i naziv za MainLog datoteku iz konfiguracije
     String logPath = "D:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\logs\\TestMonitorMain.log";

     try{
       mainLog = new MainLog(logPath);
     }catch (IOException e) {
       e.printStackTrace();
     }

    //Instanciram niti
    expectedThrdNum = 20;
    for (int i= 1; i <= expectedThrdNum; i++) {
      if (i > 4)
        monitor.waitFor();
      if (i % 2 == 0)
        new TestThrd(testThrdGrp, "notify-" + Integer.toString(i));
      else
        new TestThrd(testThrdGrp, "interrupt-" + Integer.toString(i));
      //Cekam prvu nit koja je zavrsila
    }

    //Cekam da preostale zavrse
    //waitForThrds();
  }//~TestMonitorMain()

  int notifyCount2 = 0;
  public void notified(ThreadEvent thrdEvent) {
    if (thrdEvent.getSourceThread().getName().indexOf("notify") != -1) {
      System.out.println("Notification in Main received: " + thrdEvent.toString()
                         + " Count: " + ++notifyCount2);
      monitor.monitorFrame.writeMainLog("Notification in Main received: " + thrdEvent.toString()
                                        + " Count: " + notifyCount2);
      mainLog.write(new File(thrdEvent.toString()), thrdEvent.getExitStatus(), SUCCESSFUL +
                    " Count: " + notifyCount2);
    }else {
      System.out.println("Notification in Main received: " + thrdEvent.toString()
                         + " Count: " + ++notifyCount2);
      monitor.monitorFrame.writeMainLog("Notification in Main received: " + thrdEvent.toString()
                                        + " Count: " + notifyCount2);
      mainLog.write(new File(thrdEvent.toString()), thrdEvent.getExitStatus(),"INTERRUPTED" +
                    " Count: " + notifyCount2);
      //Ako se tu izvrsi, invalidiraju se svi objekti koji su kreirani u "main" thread grupi
      //i svim podgrupama (GIU thred, event thread i svi objekti kreirani  u main metodi)
      //if (notifyCount2 >= 7) System.exit(Globals.EXIT_IOERR);
    }
    //closeResources();
  }//~notified()

  private void waitForThrds() {
    Thread[] thrdList = new Thread[testThrdGrp.activeCount()+10];
    try {
      int count = testThrdGrp.enumerate(thrdList);
      for (int i = 0; i < count; i++)
        if ((thrdList[i] != null) && (thrdList[i].isAlive())) {
            thrdList[i].join(30 * SECOND);
        }

    }catch (InterruptedException e){
      System.err.println(this.toString() + "-Nit je dobila interrupted signal");
      System.err.flush();
    }
  }//~waitForThrds()

  private void closeResources() {
    if (++currThrdNum >= expectedThrdNum)
    try{
      free();
    }catch (Exception e) {
      e.printStackTrace();
    }
  }//~closeResources()

  public void free() throws Exception {
    System.err.println("Evo me u free()");
    mainLog.close();
   System.err.println("Zatvoren log");
   System.out.println("**** Tarifiranje zavrseno u: " +
                       dateFormat.format(new java.util.Date( ) ) + " ****" );
  }//~free()

}//~class TestMonitorMain
