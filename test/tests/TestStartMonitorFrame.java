package tests;

import java.util.*;
import com.cubeprogramming.thrdmonitor.*;
import com.cubeprogramming.logs.*;

public class TestStartMonitorFrame {
  private StartMonitorFrame startMonitorFrame = null;
  public static final String LOG_DIR = "d:\\home\\cube\\Projects\\jbproject\\tarif\\cdrs\\logs\\";

  public static void main(String[] argv) {
    try {
      new TestStartMonitorFrame();
    }catch (Exception e) {
      e.printStackTrace();
    }
  }//~main()


  public TestStartMonitorFrame() throws Exception {
    startMonitorFrame = new StartMonitorFrame();
    startMonitorFrame.setOutput(true);
    startMonitorFrame.setUI(true);
    startMonitorFrame.setLogPath(LOG_DIR);
    startMonitorFrame.setLog(true);
    for (int i = 1; i < 1100; i++) {
      startMonitorFrame.writeMainLog("Main Log linija " + Integer.toString(i));
      startMonitorFrame.writeAnalysisLog("Analysis Log linija " + Integer.toString(i));
      Thread.sleep(10);
      startMonitorFrame.writeSignalsSent("Signal Send Log linija " + Integer.toString(i));
      startMonitorFrame.writeSignalsReceived("Signal Received Log linija " + Integer.toString(i));
    }
    startMonitorFrame.closeStartMonitorFrame();
  }

}//class TestStartMonitorFrame
