package tests;

import junit.framework.*;
//import tarif.threads.*;
//import tarif.utils.*;
//import tarif.*;
import com.cubeprogramming.thrdmonitor.*;
import com.cubeprogramming.utils.*;

public class TestThreadEvent
    extends TestCase implements Constants {
  private ThreadEvent thrdEvent = null;
  private ThreadGroup testThrdGrp = new ThreadGroup("testThrdGrp");

  class TestThrd extends Thread {
      public TestThrd(ThreadGroup thrdGrp,String name) {
        super(thrdGrp,name);
        this.start();
      }

      public void run() {
        try {
          sleep(Math.round(Math.random() * 10000));

        }catch (InterruptedException e){
          System.out.println("Nit je dobila interrupted signal");
        }
      }//~run()
  }//~class TestThrd


  public TestThreadEvent(String name) {
    super(name);
  }

  protected void setUp() throws Exception {
    super.setUp();
    thrdEvent = new ThreadEvent(new TestThrd(testThrdGrp, "osnovni"),
                             new ExitException(EXIT_FAILURE, "Test error") );
  }//~setUp()

  protected void tearDown() throws Exception {
    thrdEvent = null;
    super.tearDown();
  }

  public void testThrdEvent1() {
    Thread sourceThrd = null;
    try {
      ThreadEvent first = new ThreadEvent(sourceThrd);
      fail("Expected to throw but it didn't");
    }
    catch (IllegalArgumentException e) {

    }
  }//~testThrdEvent1()

  public void testThrdEvent() {
    Thread sourceThrd = new TestThrd(testThrdGrp, "prvi");
    ExitException exitErr = null;
    try {
      ThreadEvent second = new ThreadEvent(sourceThrd, exitErr);
      fail("Expected to throw but it didn't");
    }
    catch (IllegalArgumentException e) {

    }
  }//~testThrdEvent()

  public void testIsException() {
    boolean expectedReturn = true;
    boolean actualReturn = thrdEvent.isException();
    assertEquals("Exception", expectedReturn, actualReturn);
  }

  public void testIsExitException() {
    boolean expectedReturn = true;
    boolean actualReturn = thrdEvent.isExitException();
    assertEquals("Exit exception", expectedReturn, actualReturn);
  }

}//~class TestThrdEvent
