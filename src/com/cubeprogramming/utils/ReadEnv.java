/* Generated by Together */

package com.cubeprogramming.utils;

import java.util.*;
import java.io.*;

/**
 * Klasa koja slu�i za �itanje environment vatiabli operativnog sistema
 * preuzeto sa neta
 */
public class ReadEnv {
  /**
   * Stati�ka geter metoda koja vra�a environment variable
   * @return Properties objekt sa key,value parovima
   * @throws Throwable u slu�aju gre�ke kod �itanja environment variabli
   */
  public static Properties getEnvVars() throws Throwable {
    Process p = null;
    Properties envVars = new Properties();
    Runtime r = Runtime.getRuntime();
    String OS = System.getProperty("os.name").toLowerCase();

    if (OS.indexOf("windows 9") > -1)
      p = r.exec("command.com /c set");
    else if ( (OS.indexOf("nt") > -1)
             || (OS.indexOf("windows 20") > -1)
             || (OS.indexOf("windows xp") > -1))
      p = r.exec("cmd.exe /c set");
    else // our last hope, we assume Unix (thanks to H. Ware for the fix)
      p = r.exec("env");

    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
    String line;

    while ( (line = br.readLine()) != null) {
      int idx = line.indexOf('=');
      String key = line.substring(0, idx);
      String value = line.substring(idx + 1);
      envVars.setProperty(key, value);
    }

    return envVars;
  }//~getEnvVars()
}//~class ReadEnv
