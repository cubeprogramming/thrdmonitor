package com.cubeprogramming.thrdmonitor;

import java.util.*;
import com.cubeprogramming.utils.*;


/**
 * Stati�ki JavaBean objekt za monitoring threadova.
 * Prima evente iz niti i proslije�uje ih registriranim objektima
 */
public class Monitor implements Constants{
  //Staticke variable
  /**
   * Default timeout u sekundama koji �e se koristiti kod �ekanja da niti uginu.
   */
  static public final int DEFAULT_TIMEOUT = 120;

  /**
   * HashMap koji sadr�i listu svih instanciranih monitora za sve grupe niti u aplikaciji.
   */
  static private HashMap monitors = new HashMap();

  /**
   * Referenca da default Monitor.
   */
  static private Monitor defaultMonitor = null;

  static public StartMonitorFrame startMonitor = null;
  //Globalne variable
  public StartMonitorFrame monitorFrame = null;
  public long lastTimeMills = System.currentTimeMillis();

  //Lokalne variable
  private ThreadGroup thrdGroup;
  private transient ArrayList threadListeners = new ArrayList();
  private int joinTimeout = DEFAULT_TIMEOUT; //In seconds
  private int notfyCount = 0, waitCount = 0;
  private FreeResources freeResources = null;

  /**
   * Kreira Monitor objekt
   * @param thrdGroup Thread grupa kojoj pripada ovaj monitor
   */
  private Monitor(ThreadGroup thrdGroup) {
    this.thrdGroup = thrdGroup;
    Runtime currentRuntime = Runtime.getRuntime();
    currentRuntime.addShutdownHook(new ShutdownHook());
    if (startMonitor == null)
      startMonitor = new StartMonitorFrame();
    monitorFrame = startMonitor;
  }//~Monitor()

  /**
   * Vra�a instancu dafault Monitor objekta i osigurava da je samo jedan monitor kreiran za jendu grupu threadova
   * @return Monitor default instanca monitora
   */
  public synchronized static Monitor getMonitor() {
    if (defaultMonitor == null) {
      //Tra�i main grupu niti
      ThreadGroup mainGroup = Thread.currentThread().getThreadGroup();
      while (!mainGroup.getName().equals("main"))
        mainGroup = mainGroup.getParent();

      defaultMonitor = new Monitor(mainGroup);
    }
    return defaultMonitor;
  }//~getMonitor()

  /**
   * Vra�a instancu Monitor objekta i osigurava da je samo jedan monitor kreiran za jendu grupu threadova
   * @param thrdGroup Thread grupa za koju �elimo instancu monitora
   * @return Monitor default instanca monitora
   */
  public synchronized static Monitor getMonitor(ThreadGroup thrdGroup) {
    Monitor retVal = (Monitor)monitors.get(thrdGroup.toString());
    if (retVal == null) {
      retVal = new Monitor(thrdGroup);
      monitors.put(thrdGroup.toString(), retVal);
      return retVal;
    } else return retVal;
  }//~getMonitor(...)

  public synchronized long performanceMills(int numRows) {
    long currentMills = System.currentTimeMillis();
    long retVal = (currentMills - lastTimeMills) / numRows;
    lastTimeMills = currentMills;
    return retVal;
  }//~performanceMills()

  /**
   * Propertiy set metoda za postavljanje custom timeouta za �ekanje niti.
   * @param joinTimeout vrijeme u sekundama za �ekanje niti da zavr�e.
   */
  public synchronized void setJoinTimeout(int joinTimeout) {
    this.joinTimeout = joinTimeout;
  }//~setJoinTimeout()

  /**
   * Property get metoda koja vra�a trenutno postavljenu vrijednost join timeouta.
   * @return int vrjeme na koje je trenutno postavljen join timeout izra�eno u sekundama
   */
  public synchronized int getJoinTimeout() {
    return joinTimeout;
  }//~getJoinTimeout()

  /**
   * Vra�a grupu niti kojoj je pridru�en ovaj monitor
   * @return ThreadGroup grupa niti kojoj pripada ovaj monitor
   */
  public ThreadGroup getThreadGroup() {
    return thrdGroup;
  }//getThreadGroup()

  /**
   * Event metoda za registraciju svih objekata koji oslu�kuju na ThreadEvent
   * @param listener ThreadListener kojeg treba registrirati
   */
  public synchronized void addThreadListener(ThreadListener listener) {
    threadListeners.add(listener);
  }//~addThreadListener()

  /**
   * Event metoda za deregistraciju svih objekata koji oslu�kuju na ThreadEvent
   * @param listener ThreadListener kojeg treba deregistrirati
   */
  public synchronized void removeThreadListener(ThreadListener listener) {
    threadListeners.remove(listener);
  }//~removeThreadListener()


  /**
   * Event metoda za triggeriranje ThreadEvent eventa iz niti.
   * @param threadEvent koji �e bit proslije�en objektima koji �ekaju na njega
   */
  public void notifyListeners(ThreadEvent threadEvent) {

    ArrayList clonedListeners = null;
    synchronized(this){
      clonedListeners = (ArrayList)threadListeners.clone();
    }
    for (int i = 0; i < clonedListeners.size(); i++) {
      ThreadListener listener = (ThreadListener)clonedListeners.get(i);
        listener.notified(threadEvent);
    }
    monitorFrame.writeSignalsSent("Notify sent: " + threadEvent.toString() +
                                     " Count: " + ++notfyCount);
    synchronized(this){
        notifyAll();
    }
  }//~notifyListeners()


  /**
   * Metoda koja zaustavlja nit u kojoj je instanciran monitor (obi�no glavna nit) i �eka dok ne dobije signal od druge niti (u pravilu sa notifyListeners() ).
   * @return int EXIT_SUCCESS ukoliko je �ekanje normalno prekinuto ili EXIT_FAILURE ukoliko je �ekanje
   * prekinuto zbog interrupted signala.
   */
  public synchronized int waitFor() {
    try {
      super.wait();
      monitorFrame.writeSignalsReceived("Notify received: " + " Count: " + ++waitCount);
      return EXIT_SUCCESS;
    }catch (InterruptedException e) {
      return EXIT_FAILURE;
    }
  }//~waitFor()

  /**
   * Metoda koja pridru�uje referencu na objekt koji implementira FreeResources
   * interfejs
   * @param freeObject referenca na objekt koji implementira FreeResources
   */
  public void setFreeObject(FreeResources freeObject) {
    freeResources = freeObject;
  }//~setFreeObject()

  /**
   * Klasa koja osigurava da su svi threadovi zvrsili prije no sto dozvoli izlaz
   */
  class ShutdownHook extends Thread {
    public void run() {
      if (freeResources != null)
        try {
          freeResources.free();
        } catch (Exception e) {
          e.printStackTrace();
        }
      monitorFrame.closeStartMonitorFrame();
    }//~run()

  }//~class ShutdownHook

}//~class Monitor