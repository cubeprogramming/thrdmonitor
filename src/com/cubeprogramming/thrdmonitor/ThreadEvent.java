package com.cubeprogramming.thrdmonitor;

import java.util.*;
import com.cubeprogramming.utils.*;

/**
 * Custom Event koji slu�i za proslije�ivanje informacija iz niti glavnoj niti ili drugim nitima preko Monitor JavaBeana
 */
public class ThreadEvent extends EventObject implements Constants{
  //Lokalne variable
  private int exitStatus = EXIT_SUCCESS;
  private Object object = null;

  /**
   * Event op�e namjene koji se mo�e ispaliti u bilo kojem trenutku rada niti.
   * @param sourceThread Nit koja je tirgerirala event*/
  public ThreadEvent(Thread sourceThread) {
    super((Object)sourceThread);
  }//~ThrdEvent()

  /**
   * Event koji se ispaljuje na zavr�etku rada niti i �alje status izvr�enja niti.
   * @param sourceThread Nit koja je tirgerirala event
   * @param exitStatus izlazni status iz niti*/
  public ThreadEvent(Thread sourceThread, int exitStatus) {
    this(sourceThread);
    this.exitStatus = exitStatus;
  }//~ThrdEvent(...)

  /**
   * Event koji se ispaljuje kada se Exceptioni koji se pojavljuju u niti prevedu na generalni oblik izlaznog exceptiona
   * @param sourceThread Nit koja je tirgerirala event
   * @param obj Exception objekt kojeg je izbacila teku�a nit*/
  public ThreadEvent(Thread sourceThread, Object obj) {
     this(sourceThread);
     if (obj == null) throw new IllegalArgumentException("Object je null");
     object = obj;
  }//~ThrdEvent(...)

  /**
   * Event op�e namjene koji se mo�e ispaliti u bilo kojem trenutku rada niti.
   * @param sourceThread Nit koja je tirgerirala event
   * @param exitStatus izlazni status iz niti
   * @param obj generi�ki objekt kojeg se proslje�uje event listeneru*/
  public ThreadEvent(Thread sourceThread, Object obj, int exitStatus) {
     this(sourceThread,obj);
     this.exitStatus = exitStatus;
  }//~ThrdEvent(...)

  /**
   * Event koji se ispaljuje u slu�aju op�enitog exceptiona koji se pojavljuje u niti a koji nije obra�en unutar same niti.
   * @param sourceThread Nit koja je tirgerirala event
   * @param err Exception objekt kojeg je izbacila teku�a nit*/
  public ThreadEvent(Thread sourceThread, Exception err) {
     this(sourceThread,(Object)err, EXIT_FAILURE);
  }//~ThrdEvent(...)

  /**
   * Event koji se ispaljuje u slu�aju op�enitog exceptiona koji se pojavljuje u niti a koji nije obra�en unutar same niti.
   * @param sourceThread Nit koja je tirgerirala event
   * @param exitStatus izlazni status iz niti
   * @param err Exception objekt kojeg je izbacila teku�a nit*/
  public ThreadEvent(Thread sourceThread, Exception err, int exitStatus) {
     this(sourceThread,(Object)err,exitStatus);
  }//~ThrdEvent(...)

  /**
   * Event koji se ispaljuje kada se Exceptioni koji se pojavljuju u niti prevedu na generalni oblik izlaznog exceptiona
   * @param sourceThread Nit koja je tirgerirala event
   * @param exitErr Exception objekt kojeg je izbacila teku�a nit*/
  public ThreadEvent(Thread sourceThread, ExitException exitErr) {
     this(sourceThread,(Object)exitErr);
  }//~ThrdEvent(...)


  public Thread getSourceThread(){ return (Thread)getSource(); }

  public int getExitStatus(){ return exitStatus; }

  public Exception getThreadException(){
    if ((object != null) && (object instanceof Exception))
      return (Exception)object;
    return null;
  }//~getThreadException()

  public boolean isException(){ return ((object != null) && (object instanceof Exception)); }

  public boolean isExitException(){ return ((object != null) && (object instanceof ExitException));}

  public String toString() {
    //return getSourceThread().toString() + "-" + super.toString();
    return getSourceThread().toString();
  }

}//~class ThrdEvent
