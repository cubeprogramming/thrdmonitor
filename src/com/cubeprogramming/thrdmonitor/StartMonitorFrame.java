package com.cubeprogramming.thrdmonitor;

import java.awt.*;
import javax.swing.*;
import java.io.*;

/**
 * Stvara preduvjete za rad GUI okru�enja i instancira MonitorFrame
 */
public class StartMonitorFrame {
  boolean packFrame = false;
  MonitorFrame frame;
  private int buffSize;
  private String logPath = null;
  protected boolean UI = false;
  protected boolean flushToFile = false;
  private PrintWriter AnalysisLog;
  private PrintWriter SignalsSent;
  private PrintWriter SignalsReceived;
  private boolean initializedUI = false, initializedLog = false, closed = false;
  private boolean output = false;

  public StartMonitorFrame() {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

      frame = new MonitorFrame(this);

      //Pack frames that have useful preferred size info, e.g. from their layout
      //Validate frames that have preset sizes
      if (packFrame)
        frame.pack();
      else
        frame.validate();

        // Center the frame
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension frameSize = frame.getSize();
      if (frameSize.height > screenSize.height)
        frameSize.height = screenSize.height;
      if (frameSize.width > screenSize.width)
        frameSize.width = screenSize.width;
      frame.setLocation( (screenSize.width - frameSize.width) / 2,
                        ( (screenSize.height - frameSize.height) / 2) - 20);
   }catch (Exception e) {
     System.err.println("Ne mogu inicijalizirati Monitor GUI");
     UI = false;
   }

  }//~StartMonitorFrame()

  public synchronized void setUI(boolean UI) {
    this.UI = UI;
  }//~setUI()

  public synchronized boolean getUI() {
    return this.UI;
  }//~setUI()

  public synchronized void setLog(boolean log) {
    this.flushToFile = log;
  }//~setLog()

  public synchronized void setLogPath(String logPath) {
    this.logPath = logPath;
  }//~setLogPath()

  public synchronized void setOutput(boolean output) {
    this.output = output;
  }//~setLog()

  private void initializeLog() throws IOException {
    if (!initializedLog && (logPath != null)) {
      AnalysisLog = new PrintWriter(new BufferedWriter(new FileWriter(
          logPath + "Analysis.log")));
      SignalsSent = new PrintWriter(new BufferedWriter(new FileWriter(
          logPath + "SignalsSent.log")));
      SignalsReceived = new PrintWriter(new BufferedWriter(new FileWriter(
          logPath + "SignalsReceived.log")));
      initializedLog = true;
    }
  }//~initializeLog()

  private void initializeUI() {
    if (!initializedUI)
      try {
            /* buffSize = new Integer( (String) Main.DBConfig.get(Globals.SCREEN_BUFF)).
             intValue(); */
        buffSize = 1000;

     /*   UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
          frame = new MonitorFrame(this);
          //Pack frames that have useful preferred size info, e.g. from their layout
          //Validate frames that have preset sizes
          if (packFrame)
            frame.pack();
          else
            frame.validate();
            // Center the frame
          Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
          Dimension frameSize = frame.getSize();
          if (frameSize.height > screenSize.height)
            frameSize.height = screenSize.height;
          if (frameSize.width > screenSize.width)
            frameSize.width = screenSize.width;
          frame.setLocation( (screenSize.width - frameSize.width) / 2,
             ( (screenSize.height - frameSize.height) / 2) - 20);
*/
        frame.setVisible(true);
        initializedUI = true;
      }catch (Exception e) {
        System.err.println("Ne mogu inicijalizirati Monitor GUI");
        UI = false;
      }
  }//~initializeUI()

  public synchronized void writeMainLog(String line) {
   if (output) {
     if (UI) {
       initializeUI();
       int count = frame.MainLog.getLineCount();
       frame.jScrollPane1.getVerticalScrollBar().setValue(frame.MainLogIncr *
           count);
       frame.MainLog.append(line + "\n");
       if (count > buffSize)
         frame.MainLog.setText("");
     }
     else {
       System.out.println(line);
       System.out.flush();
     }
   }
  }//~writeMainLog()

  public synchronized void writeAnalysisLog(String line) {
   if (output) {
     if (UI) {
       initializeUI();
       int count = frame.AnalysisLog.getLineCount();
       frame.jScrollPane2.getVerticalScrollBar().setValue(frame.
           AnalysisLogIncr * count);
       frame.AnalysisLog.append(line + "\n");
       if (frame.AnalysisLog.getLineCount() > buffSize)
         frame.AnalysisLog.setText("");
     }
     else {
       System.out.println(line);
       System.out.flush();
     }

     try {
       if (flushToFile) {
         initializeLog();
         AnalysisLog.println(line);
       }
     }
     catch (IOException e) {
       System.err.println("Ne mogu inicijalizirati Debug Loging u datoteke");
       flushToFile = false;
     }
   }
  }//~writeAnalysisLog()

  public synchronized void writeSignalsSent(String line) {
    if (output) {
      if (UI) {
        initializeUI();
        int count = frame.SignalsSent.getLineCount();
        frame.jScrollPane3.getVerticalScrollBar().setValue(frame.
            SignalsSentIncr * count);
        frame.SignalsSent.append(line + "\n");
        if (frame.SignalsSent.getLineCount() > buffSize)
          frame.SignalsSent.setText("");
      }
      else {
        System.out.println(line);
        System.out.flush();
      }

      try {
        if (flushToFile) {
          initializeLog();
          SignalsSent.println(line);
        }
      }
      catch (IOException e) {
        System.err.println("Ne mogu inicijalizirati Debug Loging u datoteke");
        flushToFile = false;
      }
    }
  }//~writeSignalsSent()

  public synchronized void writeSignalsReceived(String line) {
    if (output) {
      if (UI) {
        initializeUI();
        int count = frame.SignalsReceived.getLineCount();
        frame.jScrollPane4.getVerticalScrollBar().setValue(frame.
            SignalsReceivedIncr * count);
        frame.SignalsReceived.append(line + "\n");
        if (frame.SignalsReceived.getLineCount() > buffSize)
          frame.SignalsReceived.setText("");
      }
      else {
        System.out.println(line);
        System.out.flush();
      }

      try {
        if (flushToFile) {
          initializeLog();
          SignalsReceived.println(line);
        }
      }
      catch (IOException e) {
        System.err.println("Ne mogu inicijalizirati Debug Loging u datoteke");
        flushToFile = false;
      }
    }
  }//~writeAnalysisLog()

  public synchronized void closeStartMonitorFrame() {
    if (!closed) {
      if (flushToFile && output) {
        AnalysisLog.close();
        SignalsSent.close();
        SignalsReceived.close();
      }
      closed = true;
    }
  }//~closeStartMonitorFrame()

}//~class StartMonitorFrame
