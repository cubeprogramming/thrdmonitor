package com.cubeprogramming.thrdmonitor;

import java.util.*;

/**
 * Interfejs koji mora biti realiziran u klasi koja �e primati evente iz Monitor Beana
 */
public interface ThreadListener extends EventListener {
  /**
   * Metoda koja mora biti realizirana u klasi primaocu eventa
   * @param e ThreadEvent objekt sa informacijama o doga�aju koji se desio
   */
  public void notified(ThreadEvent e);
}//~interface ThreadListener